#!/usr/bin/env python
# coding: utf-8

import argparse
from copy import deepcopy
from itertools import product
import json
from pathlib import Path
import re
from datetime import datetime, timedelta
import warnings
import time

from cartopy import crs
import pandas as pd
import pint
import pint.unit
import numpy as np
import xarray as xr
from distributed import Client, progress
from dask_jobqueue import SLURMCluster
import xesmf as xe
import sys
import plotly.graph_objects as go
from matplotlib import pyplot as plt
import matplotlib

matplotlib.use('Agg')

from contextlib import suppress
from distributed.diagnostics.progressbar import TextProgressBar
from distributed.diagnostics.progress import format_time

TABLE_HTML = '''<!DOCTYPE html>
<html>
<head>
<style>
    table {{
        font-family: arial, sans-serif;
        border-collapse: collapse;
       width: 100%;
    }}
    td, th {{
        border: 0px solid #ff5722;
        text-align: left;
        padding: 8px;
    }}
    tr:nth-child(even) {{
        background-color: #dddddd;
    }}
</style>
</head>
<body>
<div>
{header_1}
{table_data_1}
</div>
<div>
{header_2}
{table_data_2}
</div>
</body>
</html>
'''

class Progress:
    """Patched version of a dask-progress bar with a label."""
    def __init__(self, *args, label='', **kwargs):
        """Call dask-distributed porgress with a label."""

        TextProgressBar._draw_bar = self._draw_bar
        TextProgressBar.label = label
        TextProgressBar.percent = -1
        print(label+' :', end='', flush=True)

        try:
            progress(*args, **kwargs)
        except Exception as e:
            TextProgressBar.label = ''
            print(flush=True)
            raise e
        TextProgressBar.label = ''
        print('  done', flush=True)

    @staticmethod
    def _draw_bar(cls, remaining, all, **kwargs):
        """Monkey-Patch dask text progress bar to display bar label."""

        frac = (1 - remaining / all) if all else 1.0
        percent = int(100 * frac  / 20)
        write_msg = False
        if percent > cls.percent:
            write_msg = True
        cls.percent = percent
        if write_msg:
            with suppress(ValueError):
                print(f' ..{percent}% ', end='', flush=True)
        return

def parse_config(argv=None):
    """Construct command line argument parser."""

    argp = argparse.ArgumentParser
    ap = argp(prog='ensemble_plotter',
              description="""Plot ensemble mean and standard deviation""",
              formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    ap.add_argument('configfile', metavar='configfile', type=Path,
                    help='The json file containing the ensemble parameters')
    args = ap.parse_args()
    with open(args.configfile) as f:
        config = json.load(f)
    return config

class TimeSeriesFactory:
    """Create various time-series data."""

    def __init__(self, ensemble_data, rot_pole):

        self._dset = ensemble_data
        self.rot_pole = rot_pole
        self._yearly_cycle = None
        self._diurnal_cycle = None
        self._total_sries = None
        self.yearly_data = self.create_yearly_cycle(self._dset)

    def create_yearly_cycle(self, dset):

        years , data = zip(*dset.groupby('time.year'))
        data = list(data)
        for nn in range(len(data)):
            ts = pd.DatetimeIndex([pd.Timestamp(i).strftime('2020-%m-%dT%H%M%S')\
                    for i in data[nn].time.values])
            data[nn] = data[nn].assign_coords({'time':ts})
        return xr.concat(data, dim='year').mean(dim='year').persist()

    @property
    def yearly_cycle(self):
        """Create a yearly cycle from the data."""

        if self._yearly_cycle is not None:
            return self._yearly_cycle
        self._yearly_cycle = self.create_yearly_cycle(self.total_series)
        return self._yearly_cycle

    @property
    def total_series(self):
        """Create the normal time series of the data."""
        if self._total_sries is not None:
            return self._total_sries
        self._total_sries = self._dset.mean(dim=self._dset.dims[-2:]).persist()
        return self._total_sries

    @property
    def diurnal_cycle(self):
        """Create diurnal cycle."""
        if self._diurnal_cycle is not None:
            return self._diurnal_cycle
        dset_hour_utc = self.yearly_data.groupby('time.hour').mean()
        if self.rot_pole:
            times = [pd.Timestamp(f'2020-02-02T{h:02d}00') for h in dset_hour_utc.hour.values]
            self._diurnal_cycle = dset_hour_utc.rename({'hour':'time'}).assign_coords({'time': times})
            return self._diurnal_cycle.persist()
        self._diurnal_cycle = self._reorder(dset_hour_utc).persist()
        return self._diurnal_cycle

    def _get_time(self, group, utc):
        return (int(round((group.right - group.left)/2. + group.left, 0)) // 15 + utc.values) % 24

    def _reorder(self, dset, freq='1H', date='2020-02-02'):
        """Translate data with a utc time-step into local time"""
        local_time = {}
        time = pd.date_range(f'{date}T00:00:00', f'{date}T23:00:00', freq=freq)
        # split the earth circumference into 24 chunks -> roughly 15 degrees per hour
        groups = dset.groupby_bins('lon', np.arange(0, 360+15, 15))
        time_bins = np.array_split(np.arange(0, 24), len(time))
        for h_utc in dset.hour:
            for nn, (gr, dd) in enumerate(groups):
                lt_h = self._get_time(gr, h_utc)
                for bins in time_bins:
                    if lt_h in bins:
                        lt_bin = bins[0]
                        break
                df = dd.sel(hour=h_utc).assign_coords({'hour':[lt_bin]})
                try:
                    local_time[lt_bin].append(df)
                except (KeyError, TypeError):
                    local_time[lt_bin] = [df]
        for lt_h in local_time.keys():
            local_time[lt_h] = xr.concat(local_time[lt_h], dim='lon')
        return xr.concat(list(local_time.values()),
                        dim='hour').rename({'hour': 'time'}).assign_coords({'time':time})


class UnitConverter:

    UndefinedUnitError = pint.UndefinedUnitError
    DimensionalityError = pint.DimensionalityError

    units = pint.UnitRegistry(autoconvert_offset_to_baseunit=True)

    # Capture v0.10 NEP 18 warning on first creation
    with warnings.catch_warnings():
        warnings.simplefilter('ignore')
        units.Quantity([])

    # For pint 0.6, this is the best way to define a dimensionless unit. See pint #185
    units.define(pint.unit.UnitDefinition('percent', '%', (),
                 pint.converters.ScaleConverter(0.01)))

    units.define(pint.unit.UnitDefinition('frac', 'frac.', (),
                 pint.converters.ScaleConverter(1)))

    units.define(pint.unit.UnitDefinition('fract', 'fract.', (),
                 pint.converters.ScaleConverter(1)))

    units.define(pint.unit.UnitDefinition('1', '1.', (),
                 pint.converters.ScaleConverter(1)))

    # Define commonly encountered units not defined by pint
    units.define('degrees_north = degree = degrees_N = degreesN = degree_north = degree_N '
                 '= degreeN')
    units.define('degrees_east = degree = degrees_E = degreesE = degree_east = degree_E = degreeE')
    units._units['h'] = units._units['hour']
    # Alias geopotential meters (gpm) to just meters
    try:
        units._units['meter']._aliases = ('metre', 'gpm')
        units._units['gpm'] = units._units['meter']
    except AttributeError:
        log.warning('Failed to add gpm alias to meters.')

    # Silence UnitStrippedWarning
    if hasattr(pint, 'UnitStrippedWarning'):
        warnings.simplefilter('ignore', category=pint.UnitStrippedWarning)

    @classmethod
    def convert(cls, dset, outputunit):
        units = cls.units
        if not outputunit:
            return 1
        try:
            inputunit = dset.attrs['units']
        except KeyError:
            warnings.warn('No input unit given, cannot convet')
            return 1
        if inputunit.lower() == 'k':
            # Kelvin won't be converted just here
            return 1
        if inputunit.lower() == outputunit.lower():
            return 1
        unit = [inputunit, outputunit]
        dt = pd.Timedelta(dset.time.diff(dim='time').values[0]).total_seconds()
        # Try to convert the units to pint.units objects
        for n, uu in enumerate(unit):
            if 'frac' in unit[n] or unit[n] in ('1', '1.'):
                # Non-units assignment
                uu = ''
            elif '%' in unit[n]:
                uu = 'percent'
            else:
                # Pint cannot deal with units like s-1
                # try to identify those units and replace numeric values
                # with a '^' (if numeric characters are present but no
                # power sign is given - like in m s-1 -> m s^-1)
                for i in re.findall("[-\d]+", unit[n]):
                    if not '^' in unit[n] or '**' in unit[n]:
                        uu = uu.replace(i, f'^{i}')
            try:
                unit[n] = units(uu.replace('-^', '^-').replace('--', '-'))
            except Exception as e:
                print(f'Unit conversion failed, input unit {inputunit}, target unit {outputunit}, unit passed to pint: {unit[n]}')
                raise e
        inunit, outunit = unit
        _is_length_dim = lambda inunit: [dim[1:-1] for dim in inunit.units.dimensionality] == ['length']
        tuple_pos = lambda mul : [nn for (nn,k) in enumerate(mul.dimensionality.keys()) if k == '[time]'][0]
        if inunit == outunit:
            # If the units are euqal we do not need to do anything
            return 1
        rho_w = 1000 * units('kg/m^3') # ~ Density of water
        density_unit = units('kg/m**2').units
        if _is_length_dim(inunit) and outunit.units == density_unit:
            # Most likely this is a water based variable, hence convert
            # a water hight unit to a density unit using the density of water
            return (inunit.to_base_units() * rho_w).magnitude
        if _is_length_dim(outunit) and inunit.units == density_unit:
            # Same as above but vice versa
            conv = outunit.to_base_units() / rho_w
            return conv.to(inunit).magnitude #Convert from SI to base units
        mass_flux_unit = units('kg/m**2/s').units
        flux = (rho_w / (dt * units('s'))).to_base_units()
        speed_unit = units('meter/second').units
        if _is_length_dim(inunit) and outunit.units == mass_flux_unit:
            # Most likely this is a water based variable that needs to be
            # converted to flux unit by aplying rho_w and the time difference
            return (flux*inunit).to_base_units().magnitude
        if _is_length_dim(outunit) and inunit.units == mass_flux_unit:
            # Same flux case as above, just vice versa
            return ((outunit.to_base_units() / flux).to(inunit)).magnitude
        # Do a 'normal' magnitude conversion
        if inunit.units == density_unit and outunit.units == mass_flux_unit:
            # Input is density output mass flux -> divide by time unit
            return 1 / (dt * units('s')).to_base_units().magnitude
        if inunit.units == mass_flux_unit and outunit.units == density_unit:
            # Same as above but vice versa
            return (dt * units('s')).to_base_units().magnitude
        if inunit.to_base_units().units == speed_unit and outunit.units == mass_flux_unit:
            inunit = (inunit * rho_w).to_base_units()
        if inunit.units == mass_flux_unit and outunit.to_base_units().units == speed_unit:
            timeunit = units(outunit.to_tuple()[-1][tuple_pos(outunit)][0])
            outunit = (outunit * rho_w).to_base_units() * units('s')  / timeunit
        return inunit.to(outunit).magnitude


class EnsembleCreator:
    """Class for creating a multi-model Ensemble."""

    earth_radius = 6370000
    transform = crs.PlateCarree()
    glb = True

    def __init__(self, config, client, ensemble_name_prefix='A'):
        """Create intance for EnsembleCreator.

        config : dict
            Dictionary containg the ensemble config.
        """

        self.varname = config.pop('variable')
        start = config.pop('start')
        end = config.pop('end')
        self.client = client
        z_level = config.pop('z_level')
        time_freq = config.pop('time_frequency')
        self.linecolor = config.pop('linecolor')
        self.outdir = Path(config.pop('output_directory'))
        self.cmap = config.pop('cmap')
        self.exp = config.pop('experiment')
        self.product = config.pop('product')
        self.project = config.pop('project')
        self.proj = eval(f'crs.{config.pop("projection")}')
        lonlatbox = config.pop('lonlatbox')
        unit = config.pop('unit') or None
        ensemble = {}
        rot_pole = {}
        kwargs = dict(parallel=True, coords="minimal", data_vars="minimal",
                      compat='override', combine='nested', concat_dim='time', chunks={'time': 24})
        self.global_attrs = {}
        self.attrs = {}
        self.rot_pole = None
        self.ensemble_names = {}
        for nn, (member, files) in enumerate(config.items()):
            if not files:
                continue
            print(f'Opening files for member: {member}')
            dset = xr.open_mfdataset(files, **kwargs)
            self.ensemble_names[f'{ensemble_name_prefix}{nn+1}'] = member.split('_')
            dset = dset.assign_coords({'time': self.frac2datetime(dset.time)})
            s = np.fabs((pd.DatetimeIndex(dset.time.values) - pd.Timestamp(start or dset.time.values[0])).total_seconds())
            e = np.fabs((pd.DatetimeIndex(dset.time.values) - pd.Timestamp(end or dset.time.values[-1])).total_seconds())
            mul = UnitConverter.convert(dset[self.varname], unit)
            df = dset[self.varname].isel(time=slice(np.argmin(s), np.argmin(e)))
            if mul != 1:
                df.attrs['units'] = unit
            if df.dims[1] in ('height', 'level', 'lev', 'plev'):
                lev = 0
                if isinstance(z_level, (float, int)):
                    lev = np.argmin(np.fabs(z_level - df.coords[df.dims[1]].values))
                df = df.isel({df.dims[1]:lev})
            try:
                cb_label = df.attrs['units'], df.attrs['long_name']
                if not self.attrs:
                    self.attrs = df.attrs.copy()
            except KeyError:
                pass
            if nn == 0:
                time_range = pd.DatetimeIndex(df.time.values)
                time_range = [time_range[0], time_range[-1]]
                target_grid, dim_names = [], []
                for i in ('lat', 'lon'):
                    try:
                        target_grid.append(df[f'r{i}'].data)
                        dim_names.append(f'r{i}')
                    except KeyError:
                        target_grid.append(df[i].data)
                        dim_names.append(f'{i}')
                new_coords = dict(zip(dim_names, target_grid))
            df.data *= mul
            if unit:
                self.attrs['units'] = unit
            print(self.attrs, mul)
            ensemble[member] = df
            if not self.rot_pole:
                for rot_p in ('rotated_pole', 'rotated_latitude_longitude'):
                    try:
                        self.rot_pole = dset[rot_p].attrs
                    except KeyError:
                        pass
            else:
                dims = ensemble[member].dims[-2:]
                target = dict(zip(dims, target_grid))
                ensemble[member] = ensemble[member].reindex(method='nearest',
                                                            **target)
                ensemble[member] = ensemble[member].rename({dims[0]:dim_names[0],
                                                            dims[1]:dim_names[1]})
                ensemble[member] = ensemble[member].assign_coords(new_coords)
            if pd.Timestamp(ensemble[member].time.values[0]) > time_range[0]:
                time_range[0] = pd.Timestamp(ensemble[member].time.values[0])
            if  pd.Timestamp(ensemble[member].time.values[-1]) < time_range[-1]:
                time_range[-1] = pd.Timestamp(ensemble[member].time.values[-1])
        print('Combining Ensemble')
        self.global_attrs = {'ensemble_member':','.join(list(ensemble.keys())),
                             'time_frequency': time_freq}
        ensemble = xr.concat([ens.sel(time=slice(*time_range)) for ens in ensemble.values()],
                              dim='ensemble', coords='minimal',
                              compat='override').assign_coords({'ensemble': list(self.ensemble_names.keys())})
        self.ensemble = self.convert_variable(self.sellonlatbox(ensemble, lonlatbox))

    @property
    def cbar_label(self):
        return f'{self.attrs["long_name"]} [{self.attrs["units"]}]'

    def convert_variable(self, dset):
        """Convert certain variables to a nicer format."""
        if self.attrs['units'] == 'K': # Convert Kelvin to degC
            self.attrs['units'] = '°C'
            dset.data = dset.data - 273.15
        return dset

    @property
    def time_string(self):
        """Create string from timestep."""
        time = pd.DatetimeIndex(self.ensemble.time.values)
        return time[0].strftime('%Y%m%d') + '-' + time[-1].strftime('%Y%m%d')

    def create(self):
        """Create the actual ensemble data."""

        print("Loading Ensemble Data")
        tsf = TimeSeriesFactory(self.ensemble, self.rot_pole)
        progress([tsf.yearly_data, tsf.total_series])
        ens_mean = tsf.yearly_data.mean(dim='time').mean(dim='ensemble').persist()
        ens_std = tsf.yearly_data.mean(dim='time').std(dim='ensemble').persist()
        progress([ens_mean, ens_std])
        ens_mean = ens_mean.compute()
        ens_std = ens_std.compute()
        _data = dict(ensemble_mean=ens_mean,
                     ensemble_std=ens_std,
                     diurnal_cycle_map=None,
                     total=dict(timeseries_min=tsf.total_series.min(dim='ensemble'),
                                timeseries_max=tsf.total_series.max(dim='ensemble'),
                                timeseries_mean=tsf.total_series.mean(dim='ensemble'),
                                timeseries_std=tsf.total_series.std(dim='ensemble'),
                                ens_member=tsf.total_series),
                     yearly=dict(timeseries_min=tsf.yearly_cycle.min(dim='ensemble'),
                                 timeseries_max=tsf.yearly_cycle.max(dim='ensemble'),
                                 timeseries_mean=tsf.yearly_cycle.mean(dim='ensemble'),
                                 timeseries_std=tsf.yearly_cycle.std(dim='ensemble'),
                                 ens_member=tsf.yearly_cycle),
                     diurnal=None)
        time_steps = pd.DatetimeIndex(self.ensemble.time.values)
        if (time_steps[-1] - time_steps[0]).total_seconds() > 86400. :
            dcycle_ts = tsf.diurnal_cycle.mean(dim=self.ensemble.dims[-2:])
            _data['diurnal_cycle_map'] = tsf.diurnal_cycle.mean(dim='ensemble')
            _data['diurnal'] = dict(timeseries_min=dcycle_ts.min(dim='ensemble'),
                                    timeseries_max=dcycle_ts.max(dim='ensemble'),
                                    timeseries_mean=dcycle_ts.mean(dim='ensemble'),
                                    timeseries_std=dcycle_ts.std(dim='ensemble'),
                                    ens_member=dcycle_ts)
        _data =  self.compute(_data)
        self.client.cancel(tsf.yearly_data)
        return _data

    def compute(self, data):
        """Load all data from distributed into local memory."""

        try:
            return data.compute()
        except AttributeError:
            pass
        for key, value in data.items():
            data[key] = self.compute(value)
        return data

    @property
    def prefix(self):
        return f'{self.varname}_{self.global_attrs["time_frequency"]}_multimodel_{self.exp}'

    def save_data(self, ens_data):
        """Save the data to netcdf files.

        ens_data : dict
            ensemble data that is going to be saved.
        """

        outdir = Path(self.outdir) / 'netcdf'
        outdir.mkdir(exist_ok=True, parents=True)
        out_file = Path(outdir) / f'{self.prefix}_2d_{self.time_string}.nc'
        print(f'Saving 2d ensemble data to netcdf-file: {out_file}')
        for nn, typ in enumerate(('mean', 'std')):
            mode = 'a'
            if nn == 0:
                mode = 'w'
            data = ens_data[f'ensemble_{typ}'].compute()
            data.attrs = self.attrs
            tmp = {f'{self.varname}_ens_{typ}': data}
            if self.rot_pole:
                tmp['rotated_pole'] = xr.DataArray([], attrs=self.rot_pole)
            tmp = xr.Dataset(tmp, attrs=self.global_attrs)
            tmp.to_netcdf(out_file, engine='netcdf4', mode=mode)
        print(f'Saving time series ensemble data to netcdf-files:')
        for key in ('total', 'yearly', 'diurnal'):
            out_file = Path(outdir) / f'{self.prefix}_{key}-timeseries_{self.time_string}.nc'
            for nn, typ in enumerate(('mean', 'min', 'max', 'std')):
                mode = 'a'
                if nn == 0:
                    mode = 'w'
                data = ens_data[key][f'timeseries_{typ}']
                if data is None:
                    continue
                data = data.compute()
                data.attrs = self.attrs
                tmp = {f'{self.varname}_ens_{typ}': data}
                if self.rot_pole:
                    tmp['rotated_pole'] = xr.DataArray([], attrs=self.rot_pole)
                tmp = xr.Dataset(tmp, attrs=self.global_attrs)
                tmp.to_netcdf(out_file, mode=mode)

    def sellonlatbox(self, dataArray, box, _proj=crs.PlateCarree()):
        """Select a lonlatbox within the data array and create a map projectiong."""

        if self.rot_pole:
            pole = self.rot_pole['grid_north_pole_longitude'], self.rot_pole['grid_north_pole_latitude']
            _proj =  crs.RotatedPole(pole_longitude=pole[0], pole_latitude=pole[-1],
                                    globe=crs.Globe(semimajor_axis=self.earth_radius,
                                    semiminor_axis=self.earth_radius))
            self.proj = _proj
            self.glb = False
        if box:
            if len(box) == 4:
                xs, ys, _ = _proj.transform_points(self.transform, np.r_[box[0], box[1]],
                                                                   np.r_[box[2], box[3]]).T
                ydim, xdim = dataArray.dims[-2:]
                return dataArray.sel({ydim: slice(*list(ys)), xdim: slice(*list(xs))})
        return dataArray

    def regrid(self, ds_in, ds_out, dr_in, method):
        """Convenience function for one-time regridding"""

        regridder = xe.Regridder(ds_in, ds_out, method, periodic=True)
        dr_out = regridder(dr_in)
        regridder.clean_weight_file()
        return dr_out

    def diff_plot(self, prefix, group_1=None, group_2=None, exp1=None, exp2=None):

        for key in ('mean', 'std'):
            try:
                map_data_1 = group_1[f'ensemble_{key}'].drop('height')
            except ValueError:
                map_data_1 = group_1[f'ensemble_{key}']
            map_dims_1 = map_data_1.dims
            try:
                map_data_2 = group_2[f'ensemble_{key}'].drop('height')
            except ValueError:
                map_data_2 = group_2[f'ensemble_{key}']
            map_dims_2 = map_data_2.dims
            target = {map_dims_2[i]:map_data_1.coords[map_dims_1[i]] for i in (-2, -1)}
            rename = {map_dims_2[i]:map_dims_1[i] for i in (-2, -1)}
            map_data_2 = map_data_2.reindex(method='nearest', **target).rename(**rename)
            diff = map_data_1 - map_data_2
            xs = np.r_[min(diff[map_dims_1[-1]]), max(diff[map_dims_1[-1]])]
            ys = np.r_[min(diff[map_dims_1[-2]]), max(diff[map_dims_1[-2]])]
            if self.glb:
                transform = self.transform
            else:
                transform = self.proj
            fig = plt.figure(figsize=(12, 12))
            ax = plt.axes(projection=self.proj)
            plot = diff.plot(transform=transform,
                                 ax=ax, cmap='RdBu_r',
                                 add_colorbar=False)
            cb = plt.colorbar(plot, orientation="horizontal",
                              pad=0.02, extend='both', shrink=0.74)
            cb.set_label(self.cbar_label, fontsize=15)
            ax.coastlines('10m', color=self.linecolor, linewidth=0.8)
            ax.set_xlim(xs)
            ax.set_ylim(ys)
            ax.set_title(f'Difference for ensemble {key}: {self.varname} ({exp1} - {exp2})', fontsize=15)
            _ = fig.subplots_adjust(left=0.01, right=0.99, hspace=0.1, wspace=0.1, top=1, bottom=0.2)
            _prefix = f'{prefix}_{self.varname}_{self.global_attrs["time_frequency"]}_multimodel_'
            fname = Path(self.outdir) / f'{prefix}_{exp1}-{exp2}_{key}.png'
            fig.savefig(fname, bbox_inches='tight', format='png', dpi=150)
            ax.cla()
            fig.clf()
            plt.close()


    def plot_map(self, ens_data, prefix):
        """Plot the ensemble data."""

        for key in ('mean', 'std'):
            try:
                map_data = ens_data[f'ensemble_{key}'].drop('height')
            except ValueError:
                map_data = ens_data[f'ensemble_{key}']
            map_dims = map_data.dims
            xs = np.r_[min(map_data[map_dims[-1]]), max(map_data[map_dims[-1]])]
            ys = np.r_[min(map_data[map_dims[-2]]), max(map_data[map_dims[-2]])]
            if self.glb:
                transform = self.transform
            else:
                transform = self.proj
            fig = plt.figure(figsize=(12, 12))
            ax = plt.axes(projection=self.proj)
            plot = map_data.plot(transform=transform,
                                 ax=ax, cmap=self.cmap,
                                 add_colorbar=False)
            cb = plt.colorbar(plot, orientation="horizontal",
                              pad=0.02, extend='both', shrink=0.74)
            cb.set_label(self.cbar_label, fontsize=15)
            ax.coastlines('10m', color=self.linecolor, linewidth=0.8)
            ax.set_xlim(xs)
            ax.set_ylim(ys)
            ax.set_title(f'Ensemble {key} ({self.exp}): {self.varname} ({self.time_string.replace("-", " - ")})', fontsize=15)
            _ = fig.subplots_adjust(left=0.01, right=0.99, hspace=0.1, wspace=0.1, top=1, bottom=0.2)
            fname = Path(self.outdir) / f'{prefix}_{self.prefix}_ensemble_{key}.png'
            fig.savefig(fname, bbox_inches='tight', format='png', dpi=150)
            ax.cla()
            fig.clf()
            plt.close()

    def frac2datetime(self, icon_dates, start=None):
        """Convert datetime objects in icon format to python datetime objects.

        ::

            time = frac2datetime([20011201.5])

        Parameters:
        ==========

        icon_dates: collection
            Collection of icon date dests

        Returns:
        ========

            dates:  pd.DatetimeIndex
        """

        try:
            icon_dates = icon_dates.values
        except AttributeError:
            pass

        try:
            icon_dates = icon_dates[:]
        except TypeError:
            icon_dates = np.array([icon_dates])

        def convert(icon_date):
            frac_day, date = np.modf(icon_date)
            frac_day *= 60**2 * 24
            return datetime.strptime(str(int(date)), '%Y%m%d') + timedelta(seconds=int(frac_day.round(0)))
        conv = np.vectorize(convert)
        try:
            out = conv(icon_dates)
        except TypeError:
            try:
                out = [t.strftime('%Y%m%dT%H%M%S') for t in icon_dates]
            except AttributeError:
                out = icon_dates
        return pd.DatetimeIndex(out).round('5 min')

    def create_ts(self, ens_data, file_prefix, title='', diff_plot=False, exp1='', exp2=''):
        """Make time-series plot."""

        if not ens_data:
            return
        ens_data_ts = {}
        for typ in ('mean', 'max', 'min', 'std'):
            ens_data_ts[f'timeseries_{typ}'] = coarse_time_dim(ens_data[f'timeseries_{typ}'], variable=self.varname, unit=self.attrs['units'])
        ens_data_ts['ens_member'] = coarse_time_dim(ens_data['ens_member'], series=False, variable=self.varname, unit=self.attrs['units'])
        rows = ['mean', 'std']
        if diff_plot:
            member = ['mean']
            prefix = f'{self.varname}_{file_prefix}_difference'
            title = f'Ensemble {title} (difference: {exp1} - {exp2})'
        else:
            rows = ['mean', 'std']
            prefix = f'{file_prefix}_{self.prefix}'
            member = ['mean']+list(ens_data['ens_member'].coords['ensemble'].values)
            title = f'Ensemble {title} ({self.exp})'
        plot_array = np.array(list(product(rows, list(ens_data_ts['timeseries_mean'].keys()))))
        #type_l = {f'{k}': (self.cbar_label, '') for k in ['mean', 'std']}
        cbar_labels = {}
        fig_data = {a: [] for a in ens_data_ts['timeseries_mean'].keys()}
        btn_vis = []
        for nn, exp in enumerate(member):
            for tt, (vn, freq) in enumerate(plot_array):
                data, vunit = ens_data_ts[f'timeseries_{vn}'][freq]
                cbar_labels[freq] = f'{self.varname} [{vunit}]'
                if tt == 0:
                    visible = True
                else:
                    visible = False
                if nn == 0:
                    values = data.values
                    is_vis = visible
                    index = data.index
                    kwargs = {'name':vn, 'line_color':'tomato', 'visible':is_vis,
                             'line_width': 3}
                else:
                    pdata, _ = ens_data_ts['ens_member'][freq]
                    values = pdata.sel(ensemble=exp).values
                    index = pd.DatetimeIndex(pdata.sel(ensemble=exp).time.values)
                    if vn == 'mean':
                        is_vis = visible
                    else:
                        values *= np.nan
                        is_vis = False
                    kwargs = {'line_color':'grey', 'visible':is_vis,
                              'line_width': 0.5, 'name':f'#{str(exp)}'}
                ts = go.Scatter(x=index, y=values, mode='lines', **kwargs)
                fig_data[freq].append(ts)
        fig = go.Figure(data=fig_data[list(fig_data.keys())[0]])
        cbar_label = list(cbar_labels.values())[0]
        fig = update_layout(fig, ['mean', 'std'], fig_data, vn_lookup=cbar_labels)
        fig.update_layout(xaxis=dict(rangeslider=dict(visible=True)),
                          yaxis_title=cbar_label, title={'text': title})
        out_file = Path(self.outdir) / f'{prefix}_ensemble_timeseries.html'
        with out_file.open('w') as tf:
                tf.write(fig.to_html(out_file))




def coarse_time_dim(dset, series=True, variable='', unit=''):
    """Coarse grain time series data to various time intervals."""

    time = pd.DatetimeIndex(dset.time.values)
    dt = (time[1] - time[0]).total_seconds()
    total_time = (time[-1] - time[0]).total_seconds()
    resample_at = {600: '60S', 3600: '1h', 10800: '3h', 21600: '6h', 86400: '1d', 1296000: '15d', 3283200: '1M', 86400*366: '1Y'}
    if total_time >= 3283200:
        # Sample everything > daily if we have more than one month of data
        frequencies = (86400, 1296000, 3283200, 86400*366)
    else:
        frequencies = (600, 3600, 10800, 21600, 86400)
    new_ts = {}
    for interval in frequencies:
        if dt <= interval and total_time > interval:
            mul = 1
            if variable == 'prislt': #Convert the precip units
                if interval == 600:
                    unit = 'mm/min'
                    mul = (1/86400. * 60)
                elif interval in (3600, 10800, 21600):
                    unit = 'mm/h'
                    mul = (1/86400. * 60**2)
                elif interval >= 3283200:
                    unit = 'mm/mon'
                    mul = 30
            tmp_dset = dset.resample(time=resample_at[interval]).mean() * mul
            if series:
                new_ts[resample_at[interval]] = (pd.Series(tmp_dset.values, index=pd.DatetimeIndex(tmp_dset.time.values)), unit)
            else:
                new_ts[resample_at[interval]] = (tmp_dset, unit)
    return new_ts

def update_layout(fig,
                  varnames,
                  surftypes,
                  vn_lookup=None,
                  label_axis='yaxis',
                  times=None,
                  surf_type='buttons'):
    """Update the plotly layout and adjust ploty buttons."""

    var_buttons, surf_buttons, sliders = [], [], []
    annotations = fig.layout.annotations
    laxis = fig.layout[label_axis]
    plot_array = np.array(list(product(varnames, surftypes.keys())))
    for nn, vn in enumerate(varnames):
        visible = len(varnames) * [False]
        visible[nn] = True
        var_buttons.append(dict(label=vn, method='update',
                            args=[{'visible': visible}]))
    for surftype, data in surftypes.items():
        x, y = [ts.x for ts in data], [ts.y for ts in data]
        ann = deepcopy(laxis)
        txt = vn_lookup[surftype]
        append = dict(label=surftype,
                      method='update',
                      args=[{'y':y, 'x':x},
                            {label_axis:ann.update({'title': {'text': txt}})}])
        surf_buttons.append(append)
        sliders.append(append)
    text = vn_lookup[list(surftypes.keys())[0]]
    ann = dict(x=0, y=1.35, xanchor='left', yanchor='top', showarrow=False,
               xref='paper', yref='paper', font=dict(size=18), text=text)
    if surf_type.lower() != 'buttons':
        sliders = [dict(
            active=0,
            currentvalue={"prefix": "Time: "},
            pad={"t": 50},
            steps=sliders)]
        surf_buttons = dict()
    else:
        sliders = None
        surf_buttons = dict(
                    buttons=surf_buttons,
                    direction="down",
                    pad={"r": 10, "t": 25},
                    active=0,
                    x=0.05,
                    xanchor="left",
                    y=1.2,
                    yanchor="top"
                )
    fig.update_layout({
        label_axis:laxis.update({'title': {'text': vn_lookup[list(surftypes.keys())[0]]}}),
        'updatemenus':[
                dict(
                    buttons=var_buttons,
                    direction="down",
                    pad={"r": 10, "t": 25},
                    active=0,
                    x=0.97,
                    xanchor="left",
                    y=1.35,
                    yanchor="top"
                ),
            surf_buttons,
        ]
    }, sliders=sliders)
    return fig


def setup_cluster(account, tmp_dir, partition='gpu'):
    """Setup a dask distributed client using a slurm cluster."""

    print('Creating Dask-Distributed Cluster')
    cluster = SLURMCluster(memory='500GiB',
                           cores=72,
                           project=account,
                           walltime='08:00:00',
                           queue=partition,
                           processes=1,
                           local_directory=tmp_dir,
                           job_extra=[f'-J ensembleplotter',
                                  f'-D {tmp_dir}',
                                  f'--begin=now',
                                  f'--output={tmp_dir}/LOG_cluster.%j.o',
                                  f'--output={tmp_dir}/LOG_cluster.%j.o'
                                 ],
                       interface='ib0')
    njobs = 1
    cluster.scale(jobs=njobs)
    dask_client = Client(cluster)
    print(f'Waiting for {njobs} worker(s) ...', end='', flush=True)
    dask_client.wait_for_workers(njobs)
    print(f' done.')
    print(f'Dash board: {cluster.dashboard_link}')
    return dask_client

def ensembleplot(config, client):

    ens_data = {}
    groups = sorted([gr for gr in config.keys() if gr.startswith('group_')])
    exps = []
    html = {'header_2':'', 'table_data_2':''}
    for ng, group in enumerate(groups):
        details = {}
        with warnings.catch_warnings():
            warnings.simplefilter("ignore")
            E = EnsembleCreator(config[group], client, ensemble_name_prefix='AB'[ng])
        details['Experiment'] = E.exp
        details['Project'] = E.product
        details['Product'] = E.project
        for key, value in E.ensemble_names.items():
            details[key] = f'Model: {value[0]}, Ensemble: {value[1]}'
        df = pd.DataFrame(list(details.values()), index=details.keys(), columns=['Configuration:'])
        html[f'table_data_{ng+1}'] = df.to_html()
        html[f'header_{ng+1}'] = '<h3>Multi Model Ensemble #'+'AB'[ng]+'</h3>'
        ens_data[group] = E.create()
        E.save_data(ens_data[group])
        file_prefix = f'{ng+1}B'
        exps.append(E.exp)
        print('Plotting Data')
        E.plot_map(ens_data[group], file_prefix)
        for nk, key in enumerate(('total', 'yearly', 'diurnal')):
            if key == 'total':
                title = 'time series'
                file_prefix = f'{ng+1}{"CDE"[nk]}_timeseries_'
            else:
                title = f'{key} cycle'
                file_prefix = f'{ng+1}{"CDE"[nk]}_{key}_cycle'
            E.create_ts(ens_data[group][key], file_prefix, title=title)
    if len(groups) == 2:
        file_prefix = '3B_'
        E.diff_plot(file_prefix, **ens_data, exp1=exps[0], exp2=exps[1])
        for nk, group in enumerate(('yearly', 'diurnal')):
            file_prefix = f'3{"DE"[nk]}_{group}_cycle'
            diff_data = {}
            for key in ens_data['group_1'][group].keys():
                if ens_data['group_1'][group][key] is not None and ens_data['group_2'][group][key] is not None:
                    diff_data[key] = ens_data['group_1'][group][key] - ens_data['group_2'][group][key]
            E.create_ts(diff_data, file_prefix, title=f'{group} cycle', diff_plot=True, exp1=exps[0], exp2=exps[1])
    with (E.outdir / '1A_overview.html').open('w') as f:
        f.write(TABLE_HTML.format(**html))


def main(temp_dir, config):
    """Apply the ensemble averaging and plotting."""

    account=config.pop('account')
    da_client = Client()
    ensembleplot(config, da_client)

if __name__ == '__main__':

    from getpass import getuser
    from tempfile import NamedTemporaryFile, TemporaryDirectory
    scratch_dir = Path('/scratch') / getuser()[0] / getuser()
    with TemporaryDirectory(prefix='ensembleplotter', dir=scratch_dir) as td:
        main(td, parse_config())



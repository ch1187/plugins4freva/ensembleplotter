

import os
import sys
from pathlib2 import Path
import json
from tempfile import NamedTemporaryFile
import warnings

from evaluation_system.api import plugin, parameters
from evaluation_system.misc import config
from evaluation_system.model.file import DRSFile
from evaluation_system.model.solr import SolrFindFiles


class EnsemblePlotter(plugin.PluginAbstract):

    __category__ = 'support'
    __tags__ = ['plotting',]
    tool_developer = {'name': 'Martin Bergemann', 'email':'bergemann@dkrz.de'}
    __short_description__ = 'Create and Plot maps of multi-model ensemble means and standard deviations'
    __long_description__ = '''Calculate multi-model ensemble mean and standard deviations,
    visualise the results by plotting maps and time series.'''
    __version__ = (0,0,1)
    __parameters__ = parameters.ParameterDictionary(

            parameters.Directory(name='output_directory',
                default='$USER_OUTPUT_DIR', mandatory=True,
                help='The output directory where the animation will be save to'),
            parameters.SolrField(name='variable', mandatory=True, facet='variable',
                                 help="Variable name"),
            parameters.SolrField(name='project_1', mandatory=True, facet='project', group=1,
                                 help="Project name of the first ensemble"),
            parameters.SolrField(name='product_1', mandatory=True, facet='product', group=1,
                                 help="Product name of the first ensemble"),
            parameters.SolrField(name='experiment_1', mandatory=True, facet='experiment', group=1,
                                 help="Experiment name of the first ensemble"),
            parameters.SolrField(name='realm_1', mandatory=True, facet='realm', group=1,
                                 help="Realm name of the first ensemble"),
            parameters.SolrField(name='time_frequency_1', mandatory=True, facet='time_frequency', group=1,
                                 help="Output Frequency of the first ensemble"),
            parameters.SolrField(name='model_1', mandatory=True, facet='model', multiple=True, group=1,
                                 help="Model name(s) of the first ensemble (you can choose multiple modules)"),
            parameters.SolrField(name='ensemble_1', default=None, facet='ensemble', multiple=True, group=1,
                                 help="ensemble name(s) of the first ensemble (you can choose multiple member) if left blank all member for each model are selected"),
            parameters.String(name='start_1', default=None,
                              help='Define the first time step, leave blank if taken from data (first ensemble)'),
            parameters.String(name='end_1', default=None,
                              help='Define the last time step, leave blank if taken from data (first ensemble)'),
            parameters.Float(name='z_level_1', default=None,
                              help='Select the vertical level (in Pa or m), leave blank for 2D data only (first ensemble)'),
            parameters.Bool(name='compare_ensembles', default=False,
                            help='Set this switch to true if you want to compare a second ensemble to the first ensemble, if set to True, you will have to fill the ensemble information below.'),
            parameters.SolrField(name='project_2', default=None, facet='project', group=2,
                                 help="Project name of the second ensemble"),
            parameters.SolrField(name='product_2', default=None, facet='product', group=2,
                                 help="Product name of the second ensemble"),
            parameters.SolrField(name='experiment_2', default=None, facet='experiment', group=2,
                                 help="Experiment name of the second ensemble"),
            parameters.SolrField(name='realm_2', default=None, facet='realm', group=2,
                                 help="Realm name of the second ensemble"),
            parameters.SolrField(name='time_frequency_2', mandatory=True, facet='time_frequency', group=2,
                                 help="Output Frequency of the second ensemble"),
            parameters.SolrField(name='model_2', default=None, facet='model', multiple=True, group=2,
                                 help="Model name(s) of the second ensemble (you can choose multiple modules)"),
            parameters.SolrField(name='ensemble_2', default=None, facet='ensemble', multiple=True, group=2,
                                 help="ensemble name(s) of the second ensemble (you can choose multiple member) if left blank all member for each model are selected"),
            parameters.String(name='start_2', default=None,
                              help='Define the first time step, leave blank if taken from data (second ensemble)'),
            parameters.String(name='end_2', default=None,
                              help='Define the last time step, leave blank if taken from data (second ensemble)'),
            parameters.Float(name='z_level_2', default=None,
                              help='Select the vertical level (in Pa or m), leave blank for 2D data only (second ensemble)'),
            parameters.String(name='lonlatbox', default=None,
                              help='Set the extend of a rectangular lonlatbox (left_lon, right_lat, lower_lat, upper_lat)'),
            parameters.String(name='output_unit', mandatory=False, default=None,
                help='Set the output unit of the variable - leave blank for no conversion. This can be useful if the unit of the input files should be converted, for example for precipitation. Note: Although many conversions are supported, by using the `pint` conversion library, the conversion does not try to guess. For precipitation for example a conversion from kg/m^2/s to mm/h would not work because the system would have to guess that pr is a water based variable and that the density of water must be involved. What would work on the other hand would be a conversion from kg/m^2/s to kg/m^2/hour, which is the same as mm/h.'),
            parameters.String(name='cmap', default='RdYlBu_r',
                    help='Set the colormap for the plot, please refer to the <a href="https://matplotlib.org/stable/tutorials/colors/colormaps.html" target=_blank>matplotlib colormap page</a> for more information.'),
            parameters.String(name='projection', default='Mollweide(50)',
                              help='Set the global map projection for plotting. Note: this should be a string representation of a cartopy call that is evaluated in the call (e.g PlatteCarree(50) for Cylindrical Projection centered 50 deg east. Pleas refer to https://scitools.org.uk/cartopy/docs/latest/crs/projections.html for details.'),
            parameters.String(name='linecolor', default='k',
                              help='Color of the coast lines in the map, this can be useful if you want to choose a dark colormap and make the coastlines that appear also in dark visible.'),
            )

    def _get_files_for_group(self, config_dict, group):


        models = [f.strip() for f in config_dict['model_{}'.format(group)].split(',') if f.strip()]
        files = {}
        ensemble = config_dict['ensemble_%s'%group]
        if ensemble == '*':
            ensemble = None
        config_dict['ensemble_%s'%group] = ensemble or ' '
        for model in models:
            search_kw = dict(variable=config_dict['variable'],
                             project=config_dict['project_%s'%group],
                             product=config_dict['product_%s'%group],
                             time_frequency=config_dict['time_frequency_%s'%group],
                             experiment=config_dict['experiment_%s'%group],
                             realm=config_dict['realm_%s'%group],
                             model=model
                             )
            ens_search = [ens.strip() for ens in config_dict['ensemble_%s'%group].split(',') if ens.strip()]
            if not ens_search:
                s = SolrFindFiles
                experiment_facets = s.facets(**search_kw)
                ens_search = [ens for ens in experiment_facets['ensemble'][::2]]
            for ensemble in ens_search:
                search_kw['ensemble'] = ensemble
                res = list(DRSFile.solr_search(path_only=True, **search_kw))
                if res :
                    files["{}_{}".format(model, ensemble)] = sorted(res)
        if not files:
            raise ValueError('No files for configuration found')
        return files

    def runTool(self, config_dict=None):
        Path(config_dict['output_directory']).mkdir(exist_ok=True, parents=True)
        cfg = {'group_1': self._get_files_for_group(config_dict, 1)}
        groups = ('group_1',)
        if config_dict.pop('compare_ensembles'):
            groups += ('group_2',)
            try:
                cfg['group_2'] = self._get_files_for_group(config_dict, 2)
            except ValueError:
                warnings.warn('No files for 2nd configuration found, skipping comparison')
                groups = ('group_1',)
        for i, group in enumerate(groups):
            for key in ('linecolor', 'start', 'end', 'cmap', 'projection', 'z_level',
                        'variable', 'output_directory', 'time_frequency', 'experiment',
                         'project', 'product'):
                try:
                    cfg[group][key] = config_dict['%s_%i'%(key, i+1)]
                except KeyError:
                    cfg[group][key] = config_dict[key]
                cfg[group]['unit'] = config_dict['output_unit'] or None
            try:
                lonlatbox = config_dict['lonlatbox'].replace(';',',').replace(':', ',').strip()
                lonlatbox = [float(f.strip()) for f in lonlatbox.split(',') if f.strip()]
            except AttributeError:
                lonlatbox = None
            cfg[group]['lonlatbox'] = lonlatbox
        cfg['account'] = config.get_section('scheduler_options')['option_account']
        python_env = '/work/ch1187/regiklim-ces/freva/xarray/bin/python'
        tool_path = Path(__file__).parent / 'run.py'
        with NamedTemporaryFile(suffix='.json') as tf:
            with open(tf.name, 'w') as f:
                json.dump(cfg, f)
            cmd = '{} {} {}'.format(python_env, tool_path, tf.name)
            res = os.system(cmd)
            if res != 0:
                raise RuntimeError('Job failed')
        return self.prepareOutput(config_dict['output_directory'])
